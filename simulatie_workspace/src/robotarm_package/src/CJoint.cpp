/**
 * @file CJoint.cpp
 * @author Wout Hakvoort
 * @brief CJoint-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "CJoint.hpp"
#include <math.h>
#include <cfloat>
#include <iostream>
#include <ros/ros.h>

CJoint::CJoint():
    mCurrentPos(0.0), mDesiredPos(0.0), mStepSize(0.0)
{
    
}

CJoint::~CJoint()
{

}

bool CJoint::onPos()
{
    return fabs(mCurrentPos - mDesiredPos) < 0.005;
}

void CJoint::undoStepSize()
{
    mStepSize = 0.0;
}

void CJoint::setStepSize(int32_t aSpeed, uint16_t aDuration)
{
    double lDurationGradPerSec = (mDesiredPos - mCurrentPos)/static_cast<double>(aDuration) * 1000.0;
    std::cout << "Speed: " << aSpeed << " Time: " << lDurationGradPerSec << std::endl;
    if(aSpeed != -1)
    {
        if(aSpeed > lDurationGradPerSec)
        {
            mStepSize = (mDesiredPos - mCurrentPos) / static_cast<double>(aDuration) * 10.0;
        }
        else
        {
            mStepSize = aSpeed / 100.0;
        }
    }
    else
    {
        mStepSize = (mDesiredPos - mCurrentPos) / static_cast<double>(aDuration) * 10.0;
    }
}

void CJoint::stepIteration()
{
    mCurrentPos += mStepSize;
}

double CJoint::getCurrentPos() const
{
    return mCurrentPos;
}

void CJoint::setDesiredPos(double aDesPos)
{
    mDesiredPos = aDesPos;
}

double CJoint::getDesiredPos() const
{
    return mDesiredPos;
}