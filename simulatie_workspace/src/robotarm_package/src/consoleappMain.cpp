/**
 * @file consoleappMain.cpp
 * @author Wout Hakvoort
 * @brief main for the console application
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include "robotarm_package/MoveCommand.h"
#include <sstream>
#include <thread>
#include <chrono>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "demoscript");
    ros::NodeHandle lNodeHandle;
    ros::Publisher lPublisher = lNodeHandle.advertise<robotarm_package::MoveCommand>("/robot_arm/move", 1000);
    robotarm_package::MoveCommand lMessage;
    ros::Rate lRate(0.5);
    while(ros::ok())
    {
        std::string message ="";
        std::cin >> message;
        message += "\r";
        std::stringstream lSS;
        lSS << message;
        lMessage.command = lSS.str();
        lPublisher.publish(lMessage);
        ros::spinOnce();
        lRate.sleep();
    }
    return 0;
}