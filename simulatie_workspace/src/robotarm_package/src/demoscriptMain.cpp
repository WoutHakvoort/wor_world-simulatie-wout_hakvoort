/**
 * @file demoscriptMain.cpp
 * @author Wout Hakvoort
 * @brief demoscript-main 
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include "robotarm_package/MoveCommand.h"
#include <sstream>
#include <thread>
#include <chrono>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "demoscript");
    ros::NodeHandle lNodeHandle;
    ros::Publisher lPublisher = lNodeHandle.advertise<robotarm_package::MoveCommand>("/robot_arm/move", 1000);
    robotarm_package::MoveCommand lMessage;
    ros::Rate lRate(0.5);
    int counter = 1;
    while(ros::ok())
    {
        std::string message = "";
        std::stringstream lSS;
        switch(counter)
        {
            case 1:
            {
                sleep(3.5);
                message = "#0P1500#1P800#2P1500#3P1800#4P1500#5P500T2000";
                break;
            }
            case 2:
            {
                sleep(3.5);
                message = "#5P1500T2000";
                break;
            }
            case 3:
            {
                sleep(3.5);
                message = "#0P500#1P1200#2P1500#3P1500#4P1500T2000";
                break;
            }
            case 4:
            {
                sleep(3.5);
                message = "#0P2500#3P1500T2000";
                break;
            }
            case 5:
            {
                sleep(3.5);
                message = "#0P1500#1P1000#2P1500#3P1800#4P1500T2000";
                break;
            }
            case 6:
            {
                sleep(3.5);
                message = "#5P500T2000";
                break;
            }
            case 7:
            {
                sleep(3.5);
                message = "#0P1500#1P1500#2P500#3P1500#4P1500#5P500T2000";
                break;
            }
            default:
            {
                counter = 0;
                break;
            }
        }
        lSS << message;
        lMessage.command = lSS.str();
        lSS.clear();
        lPublisher.publish(lMessage);
        ++counter;
        ros::spinOnce();
    }
    return 0;
}