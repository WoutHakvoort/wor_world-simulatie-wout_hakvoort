/**
 * @file CCommandParser.cpp
 * @author Wout Hakvoort
 * @brief CCommandParser-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "CCommandParser.hpp"
#include <ros/ros.h>
#include <regex>
#include <cstring>
#include <sstream>

CCommandParser::CCommandParser()
{

}

CCommandParser::~CCommandParser()
{

}

SCommandMessage CCommandParser::parseCommand(const std::string& aCommand)
{
    SCommandMessage lReturnCommandMessage;

    try
    {
        // Parsing all Servo instructions from the string
        std::string lRegexString = aCommand;
        std::vector<std::string> lRegexSubstrings;
        std::smatch lMatches;
        std::regex lRegexCommand("#[A-Za-z0-9]*");
        while (std::regex_search(lRegexString, lMatches, lRegexCommand))
        {
            lRegexSubstrings.push_back(lMatches.str());
            lRegexString = lMatches.suffix().str();
            for (unsigned int i = 0; i < lMatches.str().length(); ++i)
            {
                if (lMatches.str()[i] == '\r')
                {
                    lRegexString.clear();
                    break;
                }
            }
        }

        std::smatch lServoCommandMatches;
        std::regex lRegexServoCommand("P");
        for (unsigned int i = 0; i < lRegexSubstrings.size(); ++i)
        {
            std::regex_search(lRegexSubstrings[i], lServoCommandMatches, lRegexServoCommand);
            SServoCommand lServoCommand;

            // Removes Hashtag from String and adds mServoId to the lServoCommand
            std::string lServoIdPrefix = lServoCommandMatches.prefix().str();
            lServoIdPrefix.erase(0, 1);
            std::stringstream lStringStream(lServoIdPrefix);
            uint8_t lServoId = 0;
            lStringStream >> lServoId;
            lServoCommand.mServoId = lServoId;

            //Check if speed is given
            std::smatch lSpeedCommandMatch;
            std::regex lRegexSpeedCommand("S");
            std::string lAfterPosition = lServoCommandMatches.suffix().str();
            if (std::regex_search(lAfterPosition, lSpeedCommandMatch, lRegexSpeedCommand) && i != lRegexSubstrings.size() - 1)
            {
                lServoCommand.mSpeed = stoi(lSpeedCommandMatch.suffix().str());
            }
            else if (std::regex_search(lAfterPosition, lSpeedCommandMatch, lRegexSpeedCommand) && i == lRegexSubstrings.size() - 1)
            {
                lServoCommand.mDegrees = stoi(lSpeedCommandMatch.prefix().str());
                std::smatch lSuffixMatch;
                std::regex lRegexTime("T");
                std::string lSuffixStringMatch = lSpeedCommandMatch.suffix().str();
                std::regex_search(lSuffixStringMatch, lSuffixMatch, lRegexTime);
                lServoCommand.mSpeed = stoi(lSuffixMatch.prefix().str());
                std::string lSuffix = lSuffixMatch.suffix().str();
                lReturnCommandMessage.mDuration = stoi(lSuffix);
            }
            else
            {
                if (i != lRegexSubstrings.size() - 1)
                {
                    lServoCommand.mDegrees = stoi(lServoCommandMatches.suffix().str());
                }
                else
                {
                    std::smatch lSuffixMatch;
                    std::regex lRegexTime("T");
                    std::string lSuffixStringMatch = lServoCommandMatches.suffix().str();
                    std::regex_search(lSuffixStringMatch, lSuffixMatch, lRegexTime);
                    lServoCommand.mDegrees = stoi(lSuffixMatch.prefix().str());
                    std::string lSuffix = lSuffixMatch.suffix().str();
                    lReturnCommandMessage.mDuration = stoi(lSuffix);
                }
            }

            // Add servo command to the lReturnCommandMessage
            lReturnCommandMessage.mServoCommands.push_back(lServoCommand);
        }

        // Parsing given PWM to Deg
        // mDegrees is first set as PWM val and will now be parsed to Deg
        for (unsigned int i = 0; i < lReturnCommandMessage.mServoCommands.size(); ++i)
        {
            SServoCommand lCmd = lReturnCommandMessage.mServoCommands[i];
            // UInt Hex to ASCII unsigned int 48 = 0 so 48-48 is 0
            unsigned int lServo = lCmd.mServoId - 48;
            lReturnCommandMessage.mServoCommands[i].mDegrees = mapPWMToDeg(lCmd.mDegrees, lServo);
            if (lReturnCommandMessage.mServoCommands[i].mSpeed != -1)
            {
                lReturnCommandMessage.mServoCommands[i].mSpeed = mapSpeedToGradPerSec(lCmd.mSpeed, lServo);
            }
        }

        // If Duration is less then MIN_DURATION (500), set to MIN_DURATION
        // This is set to display the physical capabilites of the Lynxmotion AL5D
        if (lReturnCommandMessage.mDuration < MIN_DURATION)
        {
            lReturnCommandMessage.mDuration = MIN_DURATION;
        }
    }
    catch (const std::exception &e)
    {
        ROS_ERROR("%s", e.what());
    }

    return lReturnCommandMessage;
}

int32_t CCommandParser::mapSpeedToGradPerSec(int32_t aSpeed, unsigned int aServoId)
{
    double lDiff = abs(mJointLowerBoundaries[aServoId]-mJointUpperBoundaries[aServoId]);
    double lCalc = lDiff/2000.0 * aSpeed;
    return static_cast<int32_t>(lCalc);
}

int16_t CCommandParser::mapPWMToDeg(int16_t aPwm, unsigned int aServoId)
{
    int16_t lReturnVal = 0;
    if (aPwm < LOWER_BOUND_PWM)
    {
        // If given PWM is smaller then LOWER_BOUND - set to lowest bound in DEG
        lReturnVal = mJointLowerBoundaries[aServoId];
    }
    else if (aPwm > UPPER_BOUND_PWM)
    {
        // If given PWM is greater then UPPER_BOUND - set to upper bound in DEG
        lReturnVal = mJointUpperBoundaries[aServoId];
    }
    else
    {
        lReturnVal = (aPwm - LOWER_BOUND_PWM) * (mJointUpperBoundaries[aServoId] - mJointLowerBoundaries[aServoId]) / (UPPER_BOUND_PWM - LOWER_BOUND_PWM) + mJointLowerBoundaries[aServoId];
    }
    return lReturnVal;
}