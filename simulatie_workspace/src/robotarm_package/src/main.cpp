/**
 * @file main.cpp
 * @author Wout Hakvoort
 * @brief robotarm_simulation main
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include "CRobotArmController.hpp"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "robotarmcontroller");
    CRobotArmController lRobotArmController;
    lRobotArmController.run();
    return 0;
}