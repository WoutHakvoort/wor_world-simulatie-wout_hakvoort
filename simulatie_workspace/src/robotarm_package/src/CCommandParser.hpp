/**
 * @file CCommandParser.hpp
 * @author Wout Hakvoort
 * @brief CCommandParser-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CCOMMANDPARSER_HPP_
#define CCOMMANDPARSER_HPP_

/** LOWER_BOUND_PWM lowest possible PWM Value */
#define LOWER_BOUND_PWM 500
/** UPPER_BOUND_PWM highest possible PWM Value */
#define UPPER_BOUND_PWM 2500
/** MIN_DURATION minimal move duration */
#define MIN_DURATION 500

#include <string>
#include <vector>
#include <cstdint>

/**
 * @brief SServoCommand-struct for one ServoCommand
 * 
 */
struct SServoCommand
{
    uint8_t mServoId;
    int16_t mDegrees;
    int32_t mSpeed = -1;
};

/**
 * @brief SCommandMessage-struct for all ServoCommands
 * 
 */
struct SCommandMessage
{
    std::vector<SServoCommand> mServoCommands;
    uint32_t mDuration;
};

/**
 * @brief CCommandParser-class
 * 
 */
class CCommandParser
{
    public:
        /**
         * @brief Construct a new CCommandParser object
         * 
         */
        CCommandParser();
        /**
         * @brief Destroy the CCommandParser object
         * 
         */
        virtual ~CCommandParser();
        /**
         * @brief Function which parses a given command
         * 
         * @param aCommand given cmd 
         * @return SCommandMessage SCommandMessage-struct containing all servocommands
         */
        SCommandMessage parseCommand(const std::string & aCommand);
    private:
        /**
         * @brief Func which maps speed to grad per sec
         * 
         * @param aSpeed given speed
         * @param aServoId a servoid
         * @return int32_t speed in grad per s
         */
        int32_t mapSpeedToGradPerSec(int32_t aSpeed, unsigned int aServoId);
        /**
         * @brief Func which maps given pwm to a deg pos
         * 
         * @param aPwm given pwm 
         * @param aServoId a servo id
         * @return int16_t deg val
         */
        int16_t mapPWMToDeg(int16_t aPwm, unsigned int aServoId);
        /** mJointLowerBoundaries Lower boundaries for servo */
        const std::vector<int8_t> mJointLowerBoundaries = {-90, -90, -90, -90, -90, -1};
        /** mJointUpperBoundaries Upper boundaries for servo */
        const std::vector<int8_t> mJointUpperBoundaries = {90, 30, 0, 90, 90, 1};
};

#endif /* CCOMMANDPARSER_HPP_ */