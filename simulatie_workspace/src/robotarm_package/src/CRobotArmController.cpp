/**
 * @file CRobotArmController.cpp
 * @author Wout Hakvoort
 * @brief CRobotArmController-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <iostream>
#include "CRobotArmController.hpp"

CRobotArmController::CRobotArmController():
    mNodeHandle(),
    mCommandSubscriber(mNodeHandle.subscribe("/robot_arm/move", 1000, &CRobotArmController::moveCallback, this)),
    mJointStatePublisher(mNodeHandle.advertise<sensor_msgs::JointState>("joint_states", 1)),
    mCupPublisher(mNodeHandle.advertise<robotarm_package::CupHeld>("/cup/grabbed", 1)),
    mTransformListener(),
    mLoopRate(100),
    mCommandParser(),
    mJoints(std::vector<CJoint>(JOINT_VEC_SIZE)),
    mMoving(false)
{
    setJointState();
}

CRobotArmController::~CRobotArmController()
{   
    
}

void CRobotArmController::run()
{
    // Run function
    while(ros::ok())
    {
        publishCupInformation();

        if(mMoving)
        {
            for(unsigned int i =0; i < mJoints.size(); ++i)
            {
                mJoints[i].stepIteration();
            }
            // mMoving is set to result of checkIfJointsOnPos
            // False if joints are on Pos --> Exiting loop
            // True if joints aren't on Pos --> Step is set
            mMoving = !checkIfJointsOnPos();
            if (!mMoving)
            {
                for (unsigned int i = 0; i < mJoints.size(); ++i)
                {
                    mJoints[i].undoStepSize();
                }
            }
        }

        updateJointState();
        publishJointState();
        ros::spinOnce();
        mLoopRate.sleep();
    }
}

void CRobotArmController::moveCallback(const robotarm_package::MoveCommand::ConstPtr& aMvCmd)
{
    const SCommandMessage lCommandMessage = mCommandParser.parseCommand(aMvCmd->command.c_str());
    if (lCommandMessage.mServoCommands.size() != 0)
    {
        for(unsigned int i = 0; i < lCommandMessage.mServoCommands.size(); ++i)
        {
            SServoCommand lCmd = lCommandMessage.mServoCommands[i];
            unsigned int lServoId = lCmd.mServoId - 48;
            if (lServoId == 5)
            {
                // Gripper
                mJoints.at(lServoId).setDesiredPos(static_cast<double>(lCmd.mDegrees));
                mJoints.at(lServoId + 1).setDesiredPos(static_cast<double>(lCmd.mDegrees));
                mJoints.at(lServoId).setStepSize(lCmd.mSpeed, lCommandMessage.mDuration);
                mJoints.at(lServoId + 1).setStepSize(lCmd.mSpeed, lCommandMessage.mDuration);
            }
            else
            {
                mJoints.at(lServoId).setDesiredPos(static_cast<double>(lCmd.mDegrees));
                mJoints.at(lServoId).setStepSize(lCmd.mSpeed, lCommandMessage.mDuration);
            }
        }
        mMoving = true;
    }
}

void CRobotArmController::setJointState()
{
    // Initialization Function for initializing the joint_states and setting them to pos 0
    mMessageJointState.name.resize(JOINT_VEC_SIZE);
    mMessageJointState.position.resize(JOINT_VEC_SIZE);
    mMessageJointState.name[0] ="base_link2turret";
    mMessageJointState.name[1] = "turret2upperarm";
    mMessageJointState.name[2] = "upperarm2forearm";
    mMessageJointState.name[3] = "forearm2wrist";
    mMessageJointState.name[4] = "wrist2hand";
    mMessageJointState.name[5] = "gripper_left2hand";
    mMessageJointState.name[6] = "gripper_right2hand";
    for(unsigned int i = 0; i < JOINT_VEC_SIZE; ++i)
    {
        mMessageJointState.position[i] = 0;
    }
}

void CRobotArmController::updateJointState()
{
    for(unsigned int i = 0; i < mJoints.size(); ++i)
    {
        mMessageJointState.position[i] = degToRad(mJoints[i].getCurrentPos());
    }
}

void CRobotArmController::publishJointState()
{
    ROS_DEBUG("CRobotArmController::publishJointState");
    mMessageJointState.header.stamp = ros::Time::now();
    mJointStatePublisher.publish(mMessageJointState);
}

bool CRobotArmController::checkIfJointsOnPos()
{
    bool lReturn = true;
    for(unsigned int i = 0; i < mJoints.size(); ++i)
    {
        if (!mJoints[i].onPos())
        {
            lReturn = false;
        }
    }
    return lReturn;
}

double CRobotArmController::degToRad(double aDeg)
{
    return aDeg * M_PI/180;
}

bool CRobotArmController::checkIfGripperHasCup()
{
    bool lReturnValue = false;
    // LeftGripper to Cup Transform
    tf::StampedTransform lLGripperTF;
    // RightGripper to Cup Transform
    tf::StampedTransform lRGripperTF;
    try
    {
        mTransformListener.lookupTransform("gripper_left", "cup", ros::Time(0), lLGripperTF);
        mTransformListener.lookupTransform("gripper_right", "cup", ros::Time(0), lRGripperTF);
        if( 
            lLGripperTF.getOrigin().z() >= 0.0 &&
            lLGripperTF.getOrigin().z() <= 0.036 &&
            lLGripperTF.getOrigin().x() >= 0.0 &&
            lLGripperTF.getOrigin().x() <= 0.068 &&
            lLGripperTF.getOrigin().y() >= -0.037 &&
            lLGripperTF.getOrigin().y() <= 0.005 &&
            lRGripperTF.getOrigin().y() >= -0.005 &&
            lRGripperTF.getOrigin().y() <= 0.037
        )
        {
            lReturnValue = true;
        }
    }
    catch(tf::TransformException &lError)
    {
        ROS_ERROR("%s", lError.what());
    }
    
    return lReturnValue;
}

void CRobotArmController::publishCupInformation()
{
    robotarm_package::CupHeld lCupHeldMsg;
    lCupHeldMsg.held = checkIfGripperHasCup();
    mCupPublisher.publish(lCupHeldMsg);
}