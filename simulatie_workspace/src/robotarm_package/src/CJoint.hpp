/**
 * @file CJoint.hpp
 * @author Wout Hakvoort
 * @brief CJoint-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CJOINT_HPP_
#define CJOINT_HPP_

#include <cstdint>

/**
 * @brief CJoint-class
 * 
 */
class CJoint
{
    public:
        /**
         * @brief Construct a new CJoint object
         * 
         */
        CJoint();
        /**
         * @brief Destroy the CJoint object
         * 
         */
        virtual ~CJoint();
        /**
         * @brief Check if a Joint is on desired pos
         * 
         * @return true if on desired pos
         * @return false if not on desired pos 
         */
        bool onPos();
        /**
         * @brief Func to setStepSize to 0.0
         * 
         */
        void undoStepSize();
        /**
         * @brief Set the Step size in Grad/Per 1/100 s depending on given speed and given duration
         *
         * @param aSpeed a Given Speed
         * @param aDuration a Given Duration
         */
        void setStepSize(int32_t aSpeed, uint16_t aDuration);
        /**
         * @brief Lets the joint make a step (currentPos+=stepsize)
         * 
         */
        void stepIteration();

        // Getters/Setters
        /**
         * @brief Get the Current Pos object
         * 
         * @return double currentpos
         */
        double getCurrentPos() const;
        /**
         * @brief Set the Desired Pos object
         * 
         * @param aDesPos desiredpos
         */
        void setDesiredPos(double aDesPos);
        /**
         * @brief Get the Desired Pos object
         * 
         * @return double desiredpos
         */
        double getDesiredPos() const;
    private:
        // All in Deg
        /** mCurrentPos currentposition, mDesiredPos desiredpostion, mStepSize stepsize */
        double mCurrentPos, mDesiredPos, mStepSize;
};

#endif /* CJOINT_HPP_ */