/**
 * @file CRobotArmController.hpp
 * @author Wout Hakvoort
 * @brief CRobotArmController-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CROBOTARMCONTROLLER_HPP_
#define CROBOTARMCONTROLLER_HPP_

/** JOINT_VEC_SIZE size of the joint vector */
#define JOINT_VEC_SIZE 7

// STL includes
#include <string>

// ROS includes
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Point.h>
#include "robotarm_package/MoveCommand.h"
#include "robotarm_package/CupHeld.h"

// Custom includes
#include "CJoint.hpp"
#include "CCommandParser.hpp"

class CRobotArmController
{
    public:
        /**
         * @brief Construct a new CRobotArmController object
         * 
         */
        CRobotArmController();
        /**
         * @brief Destroy the CRobotArmController object
         * 
         */
        virtual ~CRobotArmController();
        /**
         * @brief Run function for the CRobotArmController class
         * 
         */
        void run();
    private:
        /** mNodeHandle ros::NodeHandle object */
        ros::NodeHandle mNodeHandle;
        /** mCommandSubscriber ros::Subscriber object which is subscribed to robot_arm/move topic */
        ros::Subscriber mCommandSubscriber;
        /** mJointStatePublisher ros::Publisher object which published to the joint_states topic */
        ros::Publisher mJointStatePublisher;
        /** mCupPublisher ros::Publisher object which published to the cup/grabbed topic */
        ros::Publisher mCupPublisher;
        /** mMessageJointState object which contains the joint_states */
        sensor_msgs::JointState mMessageJointState;
        /** mTransformListener tf::TransformListener object which is used to lookup transforms */
        tf::TransformListener mTransformListener;
        /** mLoopRate ros::Rate object which contains looprate */
        ros::Rate mLoopRate;
        /** mCommandParser CCommandParser object which parses command which are given on the robot_arm/move topic */
        CCommandParser mCommandParser;
        /** mJoints std::vector<CJoint> vec-obj containing all the robotarm_joints */
        std::vector<CJoint> mJoints;
        /** mMoving bool to check if arm is moving or not */
        bool mMoving;
    private:
        /**
         * @brief Function which is called when msg is published on ros::topic robot_arm/move
         * 
         * @param aMvCmd move command message
         */
        void moveCallback(const robotarm_package::MoveCommand::ConstPtr& aMvCmd);
        /**
         * @brief Initializes joint state obj
         * 
         */
        void setJointState();
        /**
         * @brief Updates joint state ojb
         * 
         */
        void updateJointState();
        /**
         * @brief Publishes joint state msg on ros::topic joint_states
         * 
         */
        void publishJointState();
        /**
         * @brief Checks if the joints are on the desired pos
         * 
         * @return true if joints are all on pos
         * @return false if joints are not on pos
         */
        bool checkIfJointsOnPos();
        /**
         * @brief changes deg to rad
         * 
         * @param aDeg a deg value
         * @return double a rad val
         */
        double degToRad(double aDeg);
        /**
         * @brief Checks if the gripper has the cup grabbed
         * 
         * @return true if grabbed
         * @return false if not grabbed
         */
        bool checkIfGripperHasCup();
        /**
         * @brief Publishes if cup is held or not (grabbed or not)
         * 
         */
        void publishCupInformation();
};

#endif /* CROBOTARMCONTROLLER_HPP_ */