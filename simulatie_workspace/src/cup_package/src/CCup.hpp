/**
 * @file CCup.hpp
 * @author Wout Hakvoort
 * @brief CCup-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CCUP_HPP_
#define CCUP_HPP_

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_broadcaster.h>
#include "robotarm_package/CupHeld.h"
#include "cup_package/CupPos.h"
#include <tf/transform_listener.h>

/** CUP_RADIUS radius from the cup */
#define CUP_RADIUS 0.036
/** CUP_HEIGHT height from the cup */
#define CUP_HEIGHT 0.07

/**
 * @brief CCup class
 * 
 */
class CCup
{
    public:
        /**
         * @brief Construct a new CCup object
         * 
         */
        CCup();
        /**
         * @brief Destroy the CCup object
         * 
         */
        virtual ~CCup();
        /**
         * @brief Publishes the marker object
         * 
         */
        void publishMarker();
        /**
         * @brief Initializes the transform from cup to base_link so the link from base to cup is set
         * 
         */
        void initializeTransform();
        /**
         * @brief Updates the transform from the hand to the cup, used for movement when cup is held
         * 
         */
        void updateTransformHandToCup();
        /**
         * @brief Updates the transform from base_link to cup, used to publish the position
         * 
         */
        void updateTransformBaseToCup();
        /**
         * @brief Updates the transformZ, when the cup is not held
         * 
         * @param aZ a z value
         */
        void updateTransformZ(double aZ);
        /**
         * @brief Sends the transform using the TF broadcaster
         * 
         */
        void sendTransform();
        /**
         * @brief Published the cup position
         * 
         */
        void publishCupPos();
        /**
         * @brief Get the mHeld bool
         * 
         * @return true if cup is held
         * @return false if cup isnt held
         */
        bool getHeld() const;
        /**
         * @brief Get the Base To Cup T F object
         * 
         * @return tf::StampedTransform transform from base to cup, used to apply gravity
         */
        tf::StampedTransform getBaseToCupTF() const;
    private:
        /** mNodeHandle ros::NodeHandle obj */
        ros::NodeHandle mNodeHandle;
        /** mCupHeldSubscriber subscribes to cup/grabbed topic */
        ros::Subscriber mCupHeldSubscriber;
        /** mMarkerPublisher publishes marker information */
        ros::Publisher mMarkerPublisher;
        /** mCupPositionPublisher publishes cup pos */
        ros::Publisher mCupPositionPublisher;
        /** mMarker marker::Cylinder obj */
        visualization_msgs::Marker mMarker;
        /** mTransformBroadcaster broadcasts transform from cup to base, or cup to hand depending on Held-bool */
        tf::TransformBroadcaster mTransformBroadcaster;
        /** mTransformListener ros::TransformListener obj to lookup transform from base to cup or hand to cup */
        tf::TransformListener mTransformListener;
        /** mHandToCup tf::StampedTransform from hand to cup */
        tf::StampedTransform mHandToCup;
        /** mBaseToCup tf::StampedTransform from base to cup */
        tf::StampedTransform mBaseToCup;
        /** mMessageTransform geometry_msgs::TransformStamped transform msg which is broadcasted */
        geometry_msgs::TransformStamped mMessageTransform;
        /** mCurrentPos tf::Vector3 used to hold currentPos in beginning */
        tf::Vector3 mCurrentPos;
        /** mCupHeld bool, true if held, false if not held */
        bool mCupHeld;
    private:
        /**
         * @brief fnc which is called when msg is published in cup/grabbed topic 
         * 
         * @param aCupHeldCmd msg containing bool if held or not held
         */
        void cupHeldCallback(const robotarm_package::CupHeld::ConstPtr& aCupHeldCmd);
        /**
         * @brief fnc which updates color from marker obj
         * 
         * @param aR red-val
         * @param aG green-val
         * @param aB blue-val
         * @param aVal -opacity?
         */
        void updateColor(float aR, float aG, float aB, float aVal);
        /**
         * @brief fnc which updates the mMessageTransform-member
         * 
         * @param aX x
         * @param aY Y
         * @param aZ z
         * @param aRoll roll 
         * @param aPitch pitch]
         * @param aYaw yaw
         */
        void updateTransform(double aX, double aY, double aZ, double aRoll, double aPitch, double aYaw);
};

#endif /* CCUP_HPP_ */