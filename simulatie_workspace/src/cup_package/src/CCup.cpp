/**
 * @file CCup.cpp
 * @author Wout Hakvoort
 * @brief CCup-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include "CCup.hpp"

CCup::CCup():
    mNodeHandle(),
    mCupHeldSubscriber(mNodeHandle.subscribe("/cup/grabbed", 1000, &CCup::cupHeldCallback, this)),
    mMarkerPublisher(mNodeHandle.advertise<visualization_msgs::Marker>("cup_marker", 1)),
    mCupPositionPublisher(mNodeHandle.advertise<cup_package::CupPos>("/cup/position", 10)),
    mTransformListener(),
    mCurrentPos(0.45, 0.0, 0.0)
{
    mMarker.header.frame_id = "/cup";
    mMarker.ns = "cup_namespace";
    mMarker.id = 0;
    mMarker.type = visualization_msgs::Marker::CYLINDER;
    mMarker.lifetime = ros::Duration();
    mMarker.scale.x = CUP_RADIUS;
    mMarker.scale.y = CUP_RADIUS;
    mMarker.scale.z = CUP_HEIGHT;
    mMarker.color.r = 1.0f;
    mMarker.color.g = 0.0f;
    mMarker.color.b = 0.0f;
    mMarker.color.a = 1.0;
    mMarker.pose.position.x = 0.0;
    mMarker.pose.position.y = 0.0;
    mMarker.pose.position.z = 0.035;
    mMarker.pose.orientation.x = 0.0;
    mMarker.pose.orientation.y = 0.0;
    mMarker.pose.orientation.z = 0.0;
    mMarker.pose.orientation.w = 1.0;
}

CCup::~CCup()
{

}

void CCup::publishMarker()
{
    mMarker.header.stamp = ros::Time();
    mMarkerPublisher.publish(mMarker);
}

void CCup::initializeTransform()
{
    mMessageTransform.header.frame_id = "base_link";
    mMessageTransform.child_frame_id = "cup";
    mMessageTransform.transform.translation.x = mCurrentPos.getX();
    mMessageTransform.transform.translation.y = mCurrentPos.getY();
    mMessageTransform.transform.translation.z = mCurrentPos.getZ();
    mMessageTransform.transform.rotation = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, 0.0);
}

void CCup::updateTransformHandToCup()
{
    try
    {
        mTransformListener.lookupTransform("hand", "cup", ros::Time(0), mHandToCup);
    }
    catch(tf::TransformException &lError)
    {
        ROS_ERROR("%s", lError.what());
    }
}

void CCup::updateTransformBaseToCup()
{
    try
    {
        mTransformListener.lookupTransform("base_link", "cup", ros::Time(0), mBaseToCup);
    }
    catch(tf::TransformException &lError)
    {
        ROS_ERROR("%s", lError.what());
    }
}

void CCup::updateTransformZ(double aZ)
{
    mMessageTransform.transform.translation.z = aZ;
}

void CCup::sendTransform()
{
    mMessageTransform.header.stamp = ros::Time::now();
    mTransformBroadcaster.sendTransform(mMessageTransform);
}

void CCup::publishCupPos()
{
    cup_package::CupPos lCupPos;
    lCupPos.x = getBaseToCupTF().getOrigin().x();
    lCupPos.y = getBaseToCupTF().getOrigin().y();
    lCupPos.z = getBaseToCupTF().getOrigin().z();
    mCupPositionPublisher.publish(lCupPos);
}

bool CCup::getHeld() const
{
    return mCupHeld;
}

tf::StampedTransform CCup::getBaseToCupTF() const
{
    return mBaseToCup;
}

void CCup::cupHeldCallback(const robotarm_package::CupHeld::ConstPtr& aCupHeldCmd)
{
    if (mCupHeld != aCupHeldCmd->held)
    {
        mCupHeld = aCupHeldCmd->held;
        if(mCupHeld)
        {
            mMessageTransform.header.frame_id = "hand";
            updateColor(0.0f, 1.0f, 0.0f, 1.0);
            updateTransform(mHandToCup.getOrigin().x(), mHandToCup.getOrigin().y(), mHandToCup.getOrigin().z(), 0.0, -90.0 * M_PI/180.0, 0.0);
        }
        else
        {
            mMessageTransform.header.frame_id = "base_link";
            updateColor(1.0f, 0.0f, 0.0f, 1.0);
            updateTransform(mBaseToCup.getOrigin().x(), mBaseToCup.getOrigin().y(), mBaseToCup.getOrigin().z(), 0.0, 0.0, 0.0);
        }
    }    
}

void CCup::updateColor(float aR, float aG, float aB, float aVal)
{
    mMarker.color.r = aR;
    mMarker.color.g = aG;
    mMarker.color.b = aB;
    mMarker.color.a = aVal;
}

void CCup::updateTransform(double aX, double aY, double aZ, double aRoll, double aPitch, double aYaw)
{
    mMessageTransform.transform.translation.x = aX;
    mMessageTransform.transform.translation.y = aY;
    mMessageTransform.transform.translation.z = aZ;
    mMessageTransform.transform.rotation = tf::createQuaternionMsgFromRollPitchYaw(aRoll, aPitch, aYaw);
}