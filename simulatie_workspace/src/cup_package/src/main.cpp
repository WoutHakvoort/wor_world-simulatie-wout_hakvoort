/**
 * @file main.cpp
 * @author Wout Hakvoort
 * @brief cup_simulation main 
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include "CCupController.hpp"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "cupcontroller");
    CCupController lCupController;
    lCupController.run();
    return 0;
}