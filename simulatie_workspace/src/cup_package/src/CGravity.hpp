/**
 * @file CGravity.hpp
 * @author Wout Hakvoort
 * @brief CGravity-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef CGRAVITY_HPP_
#define CGRAVITY_HPP_

#define GRAVITATIONAL_MASS_PER_10_MS 9.81/100.0

/**
 * @brief CGravity-class
 * 
 */
class CGravity
{
    public:
        /**
         * @brief Construct a new CGravity object
         * 
         */
        CGravity();
        /**
         * @brief Destroy the CGravity object
         * 
         */
        virtual ~CGravity();
        /**
         * @brief Applies velocity to currentposition 
         * 
         * @param aCurrentPos current Z pos
         * @return double new current Z pos after applying velocity
         */
        double applyVelocity(double aCurrentPos);
        /**
         * @brief resets the mCurrentVelocity val to 0.0
         * 
         */
        void resetVelocity();
    private:
        /** mCurrentVelocity currentVelocity from the object */
        double mCurrentVelocity;
};

#endif /* CGRAVITY_HPP_ */