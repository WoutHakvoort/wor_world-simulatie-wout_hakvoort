/**
 * @file CCupController.hpp
 * @author Wout Hakvoort
 * @brief CCupController-header
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef CCUPCONTROLLER_HPP_
#define CCUPCONTROLLER_HPP_

#include "CCup.hpp"
#include "CGravity.hpp"

/**
 * @brief CCupController class
 * 
 */
class CCupController
{
    public:
        /**
         * @brief Construct a new CCupController object
         * 
         */
        CCupController();
        /**
         * @brief Destroy the CCupController object
         * 
         */
        virtual ~CCupController();
        /**
         * @brief CupController run func
         * 
         */
        void run();
    private:
        /** mCup CCup-object */
        CCup mCup;
        /** mGravity CGravity-object */
        CGravity mGravity;
        /** mLoopRate ros::Rate-object containing the while(ros::ok()) loop time */
        ros::Rate mLoopRate;
};

#endif /* CCUPCONTROLLER_HPP_ */