/**
 * @file CGravity.cpp
 * @author Wout Hakvoort
 * @brief CGravity-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "CGravity.hpp"

CGravity::CGravity():
    mCurrentVelocity(0.0)
{

}

CGravity::~CGravity()
{

}

double CGravity::applyVelocity(double aCurrentPos)
{
    double lCurPos = aCurrentPos;
    mCurrentVelocity += (GRAVITATIONAL_MASS_PER_10_MS/100);
    lCurPos -= mCurrentVelocity;
    if(lCurPos < 0.0)
    {
        lCurPos = 0.0;
    }
    return lCurPos;
}

void CGravity::resetVelocity()
{
    mCurrentVelocity = 0.0;
}