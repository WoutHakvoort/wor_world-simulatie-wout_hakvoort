/**
 * @file CCupController.cpp
 * @author Wout Hakvoort
 * @brief CCupController-source
 * @version 1.0
 * @date 2020-03-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include "CCupController.hpp"

CCupController::CCupController():
    mCup(),
    mGravity(),
    mLoopRate(100)
{
    mCup.initializeTransform();
}

CCupController::~CCupController()
{

}

void CCupController::run()
{
    while(ros::ok())
    {
        mCup.publishMarker();
        mCup.updateTransformHandToCup();
        mCup.updateTransformBaseToCup();
        if(!mCup.getHeld() && mCup.getBaseToCupTF().getOrigin().z() > 0.0)
        {
            mCup.updateTransformZ(mGravity.applyVelocity(mCup.getBaseToCupTF().getOrigin().z()));
        }
        else
        {
            mGravity.resetVelocity();
        }
        mCup.publishCupPos();
        mCup.sendTransform();
        ros::spinOnce();
        mLoopRate.sleep();
    }
}